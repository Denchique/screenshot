#coding:utf-8
import json,os
import tempfile

default_conf = {
		'save_path':str(os.path.join(os.path.expanduser('~'),'screengrabber_images')),
		'token':'None',
		'hot_key_region':'None',
		'hot_key_screenshot':'None',
		'token':'None',
		}

class Config:
	def __init__(self):
		self.homedir = os.path.expanduser('~')
		self.screenshots_dir = os.path.join(self.homedir,'screengrabber_images')
		self.configfile = 'screengrabber.json'
		self.config_path = os.path.join(self.homedir,self.configfile)
		self.create_config()
		self.icon_path = os.path.join(tempfile.gettempdir(),"screengrabber_icon.png")
		# self.files_count = len( [str(j) for j in os.listdir(self.get_config()['save_path']) if j.split('.')[-1] == 'JPG'] )
		self.gen_icon()
		# self.update_config(images_count=self.files_count)
	def gen_icon(self):
		imagestr = b'''iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAMAAADXqc3K
		AAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAAEwAAABMAGW7EZ5AAAAGXRFWHR
		Tb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAMBQTFRF////zP//6O
		jo5OTk5Ofn5efn5efn4+fn5Ofn5Obm5Ofn5OfnI5e/I5nBI5rCJJi/JJ3GJ
		J7HJaLMJqXQJqbRJ6HKJ6fRKJW7KZ/GK4KiLIOiLmyEMJ/EMKnRMarSMarT
		Mk1bNE9dOqXJPa/WRl9tS6/RV7fXWLfXWnF+X3aCabfRacHfarfRl6mymc3
		epdLisNTesdTeuNbfvOPxxNviyN7kzd/k09re1OHn1O311O321eHm1tve4u
		fn4+fn5OfnKLnqeQAAAAx0Uk5TAAUWMEtqf4Cy0Ob69Vq7RgAAASBJREFUK
		FN1UmtzgjAQDIiIUN9nkYI01QqttVSt2JeQ//+vvMuD2pm6HxhmN7d3twlj
		Crbjer7vuY7NLmF1AqERuNYv3+qKC3Rbhm+r43VZ1qqorc8TXxdZCpBmBWm
		BrLHI55iDRn4kN+rTwZ/DHBqkpLg4JxpVK2LCKAqXL/eQo1tgMwf19QRgGn
		POnz6/3m+hQMphLn4XeJ54/nr6+V5CJr08nGiGPsTzx4/TG3ZBL4/5QuyGA
		JEU+MPzHVaXQvgkbPuNwHlkBLSqBsYKERorap6MdXPOYyxQzWncTU+Py+Mp
		CmpcuWAy0gvKUNSCMpL9zaiJZK4jUSHuk95Y0pPVwYSoYq82yaA/nC3WVRN
		7c1HVdvf3oq5f7fXHwP55Pmc460PWfhDq/QAAAABJRU5ErkJggg=='''
		if not os.path.exists(self.icon_path):
			image = open(os.path.join(tempfile.gettempdir(),"screengrabber_icon.png"), "wb")
			image.write(imagestr.decode('base64'))
			image.close()

	def create_config(self):
		if not os.path.exists(self.config_path):
			with open(self.config_path,'w') as f:
				f.write(self.generate_default_config())

	def generate_default_config(self):
		_conf = default_conf
		if not os.path.exists(self.screenshots_dir):
			os.mkdir(self.screenshots_dir)
		_conf['save_path'] = self.screenshots_dir
		return json.dumps(_conf) # from dict to str

	def get_config(self):
		if os.path.exists(self.config_path):
			with open(self.config_path,'r') as f:
				c = f.read()
			return json.loads(c) # from str to dict

	def update_config(self,**kwargs):
		_conf = self.get_config()
		for k,v in kwargs.items():
			_conf[k] = str(v)
		with open(self.config_path,'w') as f:
			f.write(json.dumps(_conf)) # from dict to str
