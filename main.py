#coding:utf-8
import sys,os,sip
from PyQt4 import QtGui,QtCore
from PyQt4 import Qt as qt
# import screengrabber
# import goog
from PIL import Image
from base64 import decodestring
import config
import platform
import subprocess
import threading
import time
import wx_grab


conf = config.Config()
# goog.check_token()
class SystemTrayIcon(QtGui.QSystemTrayIcon):

	def __init__(self, parent=None):
		QtGui.QSystemTrayIcon.__init__(self, parent)
		menu = QtGui.QMenu(parent)
		self.icon_path = conf.icon_path
		self.setIcon(QtGui.QIcon(self.icon_path))
		self.setIcon(QtGui.QIcon(conf.icon_path))
		grabAction = menu.addAction(QtGui.QIcon('png/photo-camera.png'),"Screenshot")
		openFolderAction = menu.addAction(QtGui.QIcon('png/home.png'),"Open folder")
		optionsAction = menu.addAction(QtGui.QIcon('png/settings-1.png'),"Change folder")
		infoAction = menu.addAction(QtGui.QIcon('png/info.png'),"Info")
		exitAction = menu.addAction(QtGui.QIcon('png/error.png'),"Exit")
		self.setToolTip("tooltip")
		exitAction.triggered.connect(self.exit)
		# infoAction.triggered.connect(self.show_about)
		grabAction.triggered.connect(self.load_image)

		grabAction.setShortcut(QtGui.QKeySequence("Ctrl+a"))

		optionsAction.triggered.connect(self.set_dir)
		openFolderAction.triggered.connect(self.open_folder)
		
		self.setContextMenu(menu)
		openFolderAction.setShortcut(QtGui.QKeySequence("Ctrl+q"))


	def open_folder(self):
		if platform.system() == "Windows":
			os.startfile(conf.get_config()['save_path'])
		elif platform.system() == "Darwin":
			subprocess.Popen(["open", conf.get_config()['save_path']])
		else:
			subprocess.Popen(["xdg-open", conf.get_config()['save_path']])

	def set_dir(self):
		dir_ = QtGui.QFileDialog.getExistingDirectory(None, 'Select folder to save all screenshots:', conf.get_config()['save_path'], QtGui.QFileDialog.ShowDirsOnly)
		conf.update_config(save_path=dir_) 
		print conf.get_config()['save_path']

	def load_image(self):
		
		# grab and upload screenshot
		wx_grab.run()


	def exit(self):
		sys.exit(0)

class main(QtGui.QMainWindow):
	def __init__(self):
		
		self.app = QtGui.QApplication(sys.argv)
		super(QtGui.QMainWindow,self).__init__()
		self.w = QtGui.QWidget()
		self.trayIcon = SystemTrayIcon()
		self.trayIcon.show()
		QtGui.QShortcut(QtGui.QKeySequence("Ctrl+C"), self, None, self.trayIcon.open_folder)
		sys.exit(self.app.exec_())



if __name__ == '__main__':
	main()