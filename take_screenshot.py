import os
import datetime
import config
import test_cloud
from PIL import Image,ImageGrab

class TakeScreenshot:
	def __init__(self,bbox):
		self.x,self.y,self.ax,self.ay = bbox
		if self.x>self.ax or self.y>self.ay:
			self.screenshot((self.ax,self.ay,self.x,self.y))
		else:
			self.screenshot((self.x,self.y,self.ax,self.ay))

		self.upload()


	def upload(self):
		test_cloud.Upload(self.resultpath)
		

	def screenshot(self,bbox):
		self.resultpath = os.path.join(config.Config().get_config()['save_path'],'giba_screen%s.JPEG' %(datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))).replace('\\','/')
		self.img = ImageGrab.grab(bbox)
		self.img.save(self.resultpath, 'JPEG', quality=100)

if __name__ == '__main__':
	TakeScreenshot()